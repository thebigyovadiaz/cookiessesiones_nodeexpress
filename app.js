var express = require('express'),
	http = require('http'),
	app = express();

// Environments
app.set('port', process.env.PORT || 3030);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.bodyParser());
app.use(express.cookieParser());
app.use(express.session({'secret': 'clave secreta'}));

app.get('/', function(req, res){
	res.render('index', {
		title: '¡Hola Mundo, con Express & Jade!',
		username: 'thebigyovadiaz'
	});
});

// Comienzo de trabajo con Cookies
app.get('/name/:name', function(req, res){
	res.cookie('name', req.params.name)
	.send('<p>Vea el valor de la cookie <a href="/name">aquí</a></p>');
});

app.get('/name', function(req, res){
	res.send(req.cookies.name);
});

app.get('/clean', function(req, res){
	res.clearCookie('name');
	res.redirect('/');
});
// Culminación de trabajo con Cookies

// Comienzo de trabajo con Sesiones
app.get('/name1/:name1', function(req, res){
	req.session.name1 = req.params.name1;
	res.send('<p>Vea el valor de esta sesión <a href="/name1">aquí</a></p>');
});

app.get('/name1', function(req, res){
	res.send(req.session.name1);
});

app.get('/clean1', function(req, res){
	res.clearCookie('name1');
	res.redirect('/');
});
// Culminación de trabajo con Sesiones

app.get('/user/:userName', function(req, res){
	var name = req.params.userName;
	res.send('¡Hola ' + name + ', Bienvenido!');
});

app.post('/users', function(req, res){
	var username = req.body.username;
	res.send('¡Hola ' + username + ', Bienvenido!');
});

app.get(/\/personal\/(\d*)\/?(edit)?/, function(req, res){
	var message = 'el perfil del empleado #' + req.params[0];

	if(req.params[1] === 'edit'){
		message = 'Editando ' + message;
	} else {
		message = 'Viendo ' + message;
	}

	res.send(message);
});


http.createServer(app).listen(app.get('port'), function(){
	console.log('Express server listening on port ' + app.get('port'));
});
