# Cookies Sesiones con NodeJs - Express

	 Aplicación desarrollada con NodeJs y Express que muestra el uso de las cookies y sesiones en conjuntos con las rutas. 

## Instalación

	Luego de realizar la descarga ó clonado del repositorio, se debe:
		* Posicionarse en la carpeta y aplicar por consola (Terminal) -> ´npm install´
		* Ejecutar servidor aplicando por consola (Terminal) -> ´node app.js´
		* Abrir navegador y colocar la url -> ´localhost:3030´

		* Probar Cookies con las siguientes rutas:
			- Crear cookie -> ´localhost:3030/name/nombre´
			- Ver cookie -> ´localhost:3030/name´
			- Borrar cookie -> ´localhost:3030/clean´

		* Probar Sesiones con las siguientes rutas:
			- Crear Sesión -> ´localhost:3030/name1/nombre´
			- Ver Sesión -> ´localhost:3030/name1´
			- Borrar Sesión -> ´localhost:3030/clean1´
		
